/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-undo' : '&#xe000;',
			'icon-newspaper' : '&#xe002;',
			'icon-quill' : '&#xe003;',
			'icon-image' : '&#xe004;',
			'icon-images' : '&#xe005;',
			'icon-headphones' : '&#xe007;',
			'icon-film' : '&#xe006;',
			'icon-books' : '&#xe008;',
			'icon-profile' : '&#xe009;',
			'icon-support' : '&#xe00a;',
			'icon-location' : '&#xe00b;',
			'icon-compass' : '&#xe00c;',
			'icon-map' : '&#xe00e;',
			'icon-clock' : '&#xe00d;',
			'icon-user' : '&#xe00f;',
			'icon-zoom-out' : '&#xe010;',
			'icon-zoom-in' : '&#xe011;',
			'icon-search' : '&#xe012;',
			'icon-cog' : '&#xe013;',
			'icon-leaf' : '&#xe014;',
			'icon-list' : '&#xe015;',
			'icon-menu' : '&#xe016;',
			'icon-cloud' : '&#xe017;',
			'icon-link' : '&#xe018;',
			'icon-flag' : '&#xe019;',
			'icon-attachment' : '&#xe01a;',
			'icon-thumbs-up' : '&#xe01b;',
			'icon-thumbs-up-2' : '&#xe01c;',
			'icon-heart' : '&#xe01d;',
			'icon-star' : '&#xe01e;',
			'icon-star-2' : '&#xe01f;',
			'icon-star-3' : '&#xe020;',
			'icon-bookmark' : '&#xe021;',
			'icon-info' : '&#xe022;',
			'icon-cancel-circle' : '&#xe023;',
			'icon-checkmark-circle' : '&#xe025;',
			'icon-close' : '&#xe024;',
			'icon-checkmark' : '&#xe026;',
			'icon-play' : '&#xe027;',
			'icon-pause' : '&#xe028;',
			'icon-stop' : '&#xe029;',
			'icon-forward' : '&#xe02a;',
			'icon-backward' : '&#xe02b;',
			'icon-arrow-right' : '&#xe02c;',
			'icon-facebook' : '&#xe02f;',
			'icon-google-plus' : '&#xe02d;',
			'icon-twitter' : '&#xe02e;',
			'icon-feed' : '&#xe030;',
			'icon-vimeo' : '&#xe031;',
			'icon-flickr' : '&#xe032;',
			'icon-wordpress' : '&#xe033;',
			'icon-pinterest' : '&#xe034;',
			'icon-paypal' : '&#xe035;',
			'icon-Logo' : '&#xe036;',
			'icon-home' : '&#xe037;',
			'icon-tag' : '&#xe001;',
			'icon-envelop' : '&#xe038;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; i < els.length; i += 1) {
		el = els[i];
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};